import { Component, OnInit } from '@angular/core';
import { PlanillaService } from "../services/planilla.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-detalle-historial',
    templateUrl: './detalle-historial.component.html'
})
export class DetalleHistorialComponent {
    url: string;
    datos: any[] = [];
    total1: number = 0;
    total2: number = 0;
    total3: number = 0;
    total4: number = 0;

    constructor(private router: Router, private activatedRouter: ActivatedRoute,
        public planillaService: PlanillaService) {
        this.activatedRouter.params.subscribe(params => {
            this.url = params['idPlanilla'];
            this.planillaService.detalles(this.url)
                .subscribe(data => {
                    this.datos = data;
                    console.log(this.datos);
                    this.datos.forEach(element => {
                        console.log(element);
                        this.total1 = this.total1 + element.totalSalario;
                        this.total2 = this.total2 + element.retencion;
                        this.total3 = this.total3 + element.igss;
                        this.total4 = this.total4 + element.sueldoLiquido;
                    });
                });

        })
    }




}