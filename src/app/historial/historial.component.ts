import { Component, OnInit } from '@angular/core';
import { PlanillaService } from "../services/planilla.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {
  data: any[] = [];
  result: any[] = [];
  planilla: any = {
    _year: 0
  }
  constructor(public planillaService: PlanillaService, public router: Router) { }

  ngOnInit() {
  }

  public inicial() {
    this.planillaService.historial(this.planilla._year)
      .subscribe(data => {
        this.data = data;
        this.data.forEach(element => {
          switch (element.mes) {
            case 1:
              element.nombre = 'January';
              break;
            case 2:
              element.nombre = 'February';
              break;
            case 3:
              element.nombre = 'March';
              break;
            case 4:
              element.nombre = 'April';
              break;
            case 5:
              element.nombre = 'May';
              break;
            case 6:
              element.nombre = 'June';
              break;
            case 7:
              element.nombre = 'July';
              break;
            case 8:
              element.nombre = 'August';
              break;
            case 9:
              element.nombre = 'September';
              break;
            case 10:
              element.nombre = 'Octuber';
              break;
            case 11:
              element.nombre = 'November';
              break;
            case 12:
              element.nombre = 'December';
              break;
            default:
              element.nombre = 'ninguno';
              break;
          }

          console.log(element);
        });
      });

  }

  public borrarPlanilla(id: any) {
    console.log(id);
    this.planillaService.eliminar(id)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(['/historial']);
      })
  }

}
