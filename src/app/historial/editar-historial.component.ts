import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlanillaService } from "../services/planilla.service";

@Component({
    selector: 'app-editar-historial',
    templateUrl: './editar-historial.component.html'
})
export class EditarHistorialComponent {
    url: string;
    detalle: any = {
        totalSalario: 0,
        sueldoLiquido: 0,
        salarioOrdinario: 0,
        igss: 0,
        retencion: 0,
        idDetallePlanilla: 0
    }

    constructor(private planillaService: PlanillaService,
        private router: Router, private activatedRouter: ActivatedRoute) {
        this.activatedRouter.params.subscribe(params => {
            this.url = params["idDetallePlanilla"];
            this.planillaService.detalle(this.url)
                .subscribe(c => {
                    this.detalle = c;
                    console.log(this.detalle);
                });
        });

    }

    public guardarCambios() {
        console.log(this.detalle);
        let datos = {
            sueldoOrdinario: this.detalle.salarioOrdinario,
            retencion: this.detalle.retencion,
            igss: this.detalle.igss,
            bonificacion: this.detalle.bonificacion
        }
        console.log(datos);
        if (this.detalle.salarioOrdinario !== '' && this.detalle.igss !== ''
            && this.detalle.retencion !== '' && this.detalle.bonificacion !== '') {
            this.planillaService.update(datos, this.detalle.idDetallePlanilla)
                .subscribe(res => {
                    console.log(res);
                    this.router.navigate(['/historial']);
                })

        } else {
            alert('no se validaron campos');
        }

    }
}