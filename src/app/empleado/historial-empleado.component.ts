import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { EmpleadoService } from "../services/empleado.service";

@Component({
  selector: 'app-historial-empleado',
  templateUrl: './historial-empleado.component.html'
})
export class HistorialEmpleadoComponent {
    url: string;
    bonificaciones: any[] = [];
    retenciones: any[] = [];
    constructor(private empleadoService: EmpleadoService, 
    private router: Router, private activatedRouter: ActivatedRoute,) 
    {
        this.activatedRouter.params.subscribe(params => {
            this.url = params["idEmpleado"];
            console.log(this.url);
            this.empleadoService.getHistorial1(this.url)
            .subscribe(
                datos => {
                    this.bonificaciones = datos;
                    console.log(this.bonificaciones);
                }
            );
            this.empleadoService.getHistorial2(this.url)
            .subscribe(datos =>{
                this.retenciones = datos;
                console.log(this.retenciones);
            });

        });

    }

}