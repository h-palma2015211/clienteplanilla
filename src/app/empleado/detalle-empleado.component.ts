import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { EmpleadoService } from "../services/empleado.service";

@Component({
  selector: 'app-detalle-empleado',
  templateUrl: './detalle-empleado.component.html'
})
export class DetalleEmpleadoComponent {
    url: string;
    a: boolean = false;
    b: boolean = false;
    empleado: any = {
        nombre: '',
        apellido: '',
        fechaInicio: '',
        idEstado: 0,
        bonificacion: 0,
        retencion: 0,
        fechaInactividad: null,
        idEmpleado: 0,
        fecha1: '',
        fecha2: ''
    }
    

    constructor(private empleadoService: EmpleadoService, 
    private router: Router, private activatedRouter: ActivatedRoute,) 
    {
       this.activatedRouter.params.subscribe(params => {
            this.url = params["idEmpleado"];
            if(this.url !== "nuevo") {
                console.log(this.url);
               
                this.empleadoService.getEmpleado(this.url)
                .subscribe(c => this.empleado = c);
                

            } else {
                this.reset();
                
            }
       });
    }
    private reset() {
        this.empleado.nombre = '';
        this.empleado.apellido = '';
        this.empleado.fechaInicio = '';
        this.empleado.idEstado = 0;
        this.empleado.bonificacion = 0;
        this.empleado.retencion = 0;
        this.empleado.fechaInactividad = '';
        this.empleado.idEmpleado = 0;
        this.a = true;
    }

    estado(){
        this.b = true;
        console.log(this.b +"hola");
    }
    estado1(){
        this.b = false;
        console.log(this.b +"hola estad1");
    
            this.empleado.fechaInactividad = '0000-00-00 00:00:00';
    }


    guardarCambios() {
        if(this.url === "nuevo") {
                if(this.empleado.idEstado === 1) {
                    if(this.empleado.nombre !== '' && this.empleado.apellido !== '' && this.empleado.fechaInicio !== '' &&
                        this.empleado.idEstado !== '' && this.empleado.bonificacion !== '' && 
                        this.empleado.retencion !== ''){
                            
                            this.empleadoService.insertEmpleado(this.empleado)
                                .subscribe(
                                    res => {
                                        console.log(res);
                                        this.reset();
                                        this.router.navigate(['/empleado']);
                                    }
                                );
                    }else {
                        alert('compruebe que los campos no esten vacios');
                    }
                } else { 
                    if(this.empleado.nombre !== '' && this.empleado.apellido !== '' && this.empleado.fechaInicio !== '' &&
                        this.empleado.idEstado !== '' && this.empleado.bonificacion !== '' &&   this.empleado.retencion !== '' &&
                        this.empleado.fechaInactividad !== '' ) {
                            this.empleadoService.insertEmpleado(this.empleado)
                    .subscribe(
                        res => {
                            console.log(res);
                            this.reset();
                             this.router.navigate(['/empleado']);
                        }
                    );
                        
                } else {
                    alert('compruebe que los campos no esten vacios');
                }
                
                }
                


        } else {
            if(this.empleado.idEstado === 1) {
                if(this.empleado.nombre !== '' && this.empleado.apellido !== '' && this.empleado.fechaInicio !== '' &&
                        this.empleado.idEstado !== '' && this.empleado.bonificacion !== '' && 
                        this.empleado.retencion !== '') {
                            this.empleadoService.updateEmpleado(this.empleado, this.url)
                                .subscribe(res => {
                                    console.log(res);
                                    this.router.navigate(['/empleado']);
                                });
                } else {
                     alert('compruebe que los campos no esten vacios');
                } 
            } else {
                if( this.empleado.nombre !== '' && this.empleado.apellido !== '' && this.empleado.fechaInicio !== '' &&
                        this.empleado.idEstado !== '' && this.empleado.bonificacion !== '' &&   this.empleado.retencion !== '' &&
                        this.empleado.fechaInactividad !== '') {
                            this.empleadoService.updateEmpleado(this.empleado, this.url)
                                .subscribe(res => {
                                    console.log(res);
                                    this.router.navigate(['/empleado']);
                                 });
                } else {
                        alert('compruebe que los campos no esten vacios');


                 }

            }
            
        }
    }

}