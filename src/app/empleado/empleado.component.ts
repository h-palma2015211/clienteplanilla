import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from "../services/empleado.service";

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {
  empleados: any[] = [];
  s: boolean = false;
  constructor(
    private empleadoService: EmpleadoService
  ) { }

  ngOnInit() {
    this.inicio();
  }
  private inicio() {
    this.empleadoService.getEmpleados()
      .subscribe(datos => {
        this.empleados = datos;
        console.log(this.empleados);
        this.empleados.forEach(element => {
          console.log(element.estado);

        });
      });
  }

  private borrarEmpleado(idEmpleado: number) {
    this.empleadoService.deleteEmpleado(idEmpleado)
      .subscribe(res => {
        this.inicio();
      });
  }

}
