import { RouterModule, Routes } from '@angular/router';
import { IgssComponent } from "./igss/igss.component";
import { SalarioOrdinarioComponent } from "./salario-ordinario/salario-ordinario.component";
import { AppComponent } from "./app.component";
import { PlanillaComponent } from "./planilla/planilla.component";
import { EmpleadoComponent } from "./empleado/empleado.component";
import { HomeComponent } from "./home/home.component";
import { DetalleSalarioOrdinarioComponent } from "./salario-ordinario/detalle-salario-ordinario.component";
import { DetalleIgssComponent } from "./igss/detalle-igss.component";
import { DetalleEmpleadoComponent } from "./empleado/detalle-empleado.component";
import { HistorialEmpleadoComponent } from "./empleado/historial-empleado.component";
import { HistorialComponent } from "./historial/historial.component";
import { DetalleHistorialComponent } from "./historial/detalle-historial.component";
import { EditarHistorialComponent } from "./historial/editar-historial.component";
const APP_ROUTES: Routes = [
  { path: 'igss', component: IgssComponent},
  { path: 'igss/:idIgss', component: DetalleIgssComponent},
  { path: 'salario', component: SalarioOrdinarioComponent },
  { path: 'salario/:idSalarioOrdinario', component: DetalleSalarioOrdinarioComponent },
  { path: 'inicio', component: AppComponent },
  { path: 'home', component: HomeComponent },
  { path: 'planilla', component: PlanillaComponent },
  { path: 'empleado', component: EmpleadoComponent },
  { path: 'historial', component: HistorialComponent },
  { path: 'historial/:idPlanilla', component: DetalleHistorialComponent },
  { path: 'historial/editar/:idDetallePlanilla', component: EditarHistorialComponent },
  { path: 'empleado/:idEmpleado', component: DetalleEmpleadoComponent },
  { path: 'empleado/historial/:idEmpleado', component: HistorialEmpleadoComponent },  
  { path: '**', pathMatch: 'full', redirectTo:'home'}
]

export const app_routing = RouterModule.forRoot(APP_ROUTES);