import { Component, OnInit } from '@angular/core';
import { PlanillaService } from "../services/planilla.service";
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-planilla',
  templateUrl: './planilla.component.html',
  styleUrls: ['./planilla.component.css']
})
export class PlanillaComponent implements OnInit {
  url: string;
  fecha: any;
  planilla: any = {
    _year: 0,
    mes: 0
  }
  s: boolean = false;
  pl: any[] = [];
  generada: any[] = [];
  j:boolean = false;
  t:boolean = false;
  total1: number = 0;
  total2: number = 0;
  total3: number = 0;
  total4: number = 0;

  constructor( private planillaService: PlanillaService, private router: Router) { }

  ngOnInit() {
  }

  public iniciar() {
    this.planillaService.getPlanilla(this.planilla)
    .subscribe(data => {
        this.pl = data;
        console.log(this.pl);
        if(this.pl.length > 0){
            this.j = true;
            this.t = false;
            console.log("hola adentro del if");
            this.fecha = this.pl[0].fechaGeneracion.split('T');
            this.pl.forEach(element => {
                this.total1 = this.total1 + element.totalSalario;
                this.total2 = this.total2 + element.retencion;
                this.total3 = this.total3 + element.igss;
                this.total4 = this.total4 + element.sueldoLiquido;
            });
              
        } else {
          this.j = false;
          this.t = true;
          console.log("se debe generar");
          this.planillaService.getDatos(this.planilla)
          .subscribe( data => {

            console.log(data);
            this.generada = data;
            console.log(this.generada);
          });
        }
    });
  }

  public guardarPlanilla() {
    console.log(this.generada);
    this.planillaService.guardarPla(this.planilla)
    .subscribe(res => {
      console.log(res);
    })
    this.generada.forEach(element => {
        this.planillaService.guardarDetalle(element)
        .subscribe(res => {
          console.log(res);
          this.router.navigate(['/historial']);
        })
    });
  }

}
