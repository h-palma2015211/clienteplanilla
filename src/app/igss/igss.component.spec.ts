import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IgssComponent } from './igss.component';

describe('IgssComponent', () => {
  let component: IgssComponent;
  let fixture: ComponentFixture<IgssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IgssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IgssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
