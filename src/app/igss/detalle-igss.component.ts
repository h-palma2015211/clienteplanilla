import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { IgssService } from "../services/igss.service";

@Component({
  selector: 'app-detalle-igss',
  templateUrl: './detalle-igss.component.html'
})
export class DetalleIgssComponent implements OnInit {
  url:string;
  igss: any = {
    idIgss: 0,
    _year: 0,
    cuota: 0
  }
  
  constructor( 
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private igssService: IgssService 
  ) {
    this.activatedRouter.params.subscribe(params => {
      this.url = params["idIgss"];
      if(this.url !== "nuevo") {
        this.igssService.getUno(this.url)
        .subscribe(c => this.igss = c);
      }
    });
   }

   private resetFormulario() {
     this.igss._year = 0;
     this.igss.cuota = 0;
   }

  guardarCambios() {
    if(this.url === "nuevo"){
      
      if(this.igss._year !== '' && this.igss.cuota !== '') {
        this.igssService.insertIgss(this.igss)
          .subscribe(res => {
            console.log(res);
            this.resetFormulario();
            this.router.navigate(['/igss']);
          });
      } else {
        alert('compruebe que los campos no esten vacios');
      }
      
    } else {

      if(this.igss._year !== '' && this.igss.cuota !== '') {
      console.log(this.igss);
      this.igssService.updateIgss(this.igss, this.url)
      .subscribe(res => {
        this.router.navigate(['/igss']);
      });
       } else {
        alert('compruebe que los campos no esten vacios');
      }
    }
  }


  ngOnInit() {
  }


}