import { Component, OnInit } from '@angular/core';
import { IgssService } from "../services/igss.service";

@Component({
  selector: 'app-igss',
  templateUrl: './igss.component.html',
  styleUrls: ['./igss.component.css']
})
export class IgssComponent implements OnInit {
  url:string = "http://localhost:3001";
  ig: any[] = [];
  constructor(
    private igssService: IgssService
  ) { }

  ngOnInit() {
    this.inicializar();
  }
  private inicializar() {
    this.igssService.getIgss()
    .subscribe(data => {
      this.ig = data;
      console.log(this.ig);
    });
  }
  
  public borrarIgss(idIgss: number) {
    this.igssService.deleteIgss(idIgss)
    .subscribe(res => {
        this.inicializar();
    });
  }

}
