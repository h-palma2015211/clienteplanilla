import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalarioOrdinarioComponent } from './salario-ordinario.component';

describe('SalarioOrdinarioComponent', () => {
  let component: SalarioOrdinarioComponent;
  let fixture: ComponentFixture<SalarioOrdinarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalarioOrdinarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalarioOrdinarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
