import { Component, OnInit } from '@angular/core';
import { SalarioOrdinarioService } from "../services/salarioOrdinario.service";

@Component({
  selector: 'app-salario-ordinario',
  templateUrl: './salario-ordinario.component.html',
  styleUrls: ['./salario-ordinario.component.css']
})
export class SalarioOrdinarioComponent implements OnInit {
  salarios: any[] =[];

  constructor( private salarioService: SalarioOrdinarioService) { }

  ngOnInit() {
    this.inicializar();
  }
  private inicializar() {
    this.salarioService.getSalarios().subscribe( datos =>{
      this.salarios = datos;
      console.log(this.salarios);
    })
  }

  public borrarSalario(idSalario: number) {
      console.log(idSalario);
      this.salarioService.deleteSalario(idSalario)
      .subscribe(res => {
        this.inicializar();
      })
  }



}
