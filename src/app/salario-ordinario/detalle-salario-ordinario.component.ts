import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { SalarioOrdinarioService } from "../services/salarioOrdinario.service";

@Component({
  selector: 'app-detalle-salario-ordinario',
  templateUrl: './detalle-salario-ordinario-component.html'
})
export class DetalleSalarioOrdinarioComponent implements OnInit {
  url: string;
  salarioOrdinario: any = {
      idSalarioOrdinario: 0,
      _year: 0,
      salario: 0
  }
  constructor( private salarioService: SalarioOrdinarioService,
  private router: Router, private activatedRouter: ActivatedRoute) {
    this.activatedRouter.params.subscribe( params => {
        this.url = params["idSalarioOrdinario"];
        if(this.url !== "nuevo" ) {
            console.log(this.url);
            this.salarioService.getSalario(this.url)
            .subscribe(c => this.salarioOrdinario = c);
        } else {
            this.resetFormulario();
        }
    })

   }
   private resetFormulario() {
       this.salarioOrdinario._year = 0;
       this.salarioOrdinario.salario = 0;
   }


   guardarCambios() {
        if(this.url === "nuevo") {
            if(this.salarioOrdinario._year !== '' && this.salarioOrdinario.salario !== ''){
                this.salarioService.insertSalario(this.salarioOrdinario)
                    .subscribe(res => {
                        console.log(res);
                        this.resetFormulario();
                        this.router.navigate(['/salario']);
                    })
            } else {
                alert("campos vacios");
            }
            
        } else {
            if(this.salarioOrdinario._year !== '' && this.salarioOrdinario.salario !== ''){
            console.log(this.salarioOrdinario);
            this.salarioService.updateSalario(this.salarioOrdinario, this.url)
            .subscribe(res => {
                console.log(res);
                this.router.navigate(['/salario']);
            })
            } else {
                alert("campos vacios");
            }    

        }

   }
  ngOnInit() {
 
  }
  

 



}
