import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class IgssService {
    url: string = "http://localhost:3001";
    constructor(private http: Http) { }

    public getIgss() {
        let uri = `${this.url}/igss`;
        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public getUno(id: any) {
        let uri = this.url + "/igss/" + id;
        console.log(uri);
        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public insertIgss(igss: any) {
        let uri = this.url + "/igss";
        let data = JSON.stringify(igss);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.post(uri, data, { headers })
            .map(res => {
                return res.json();
            });
    }

    public updateIgss(igss: any, idIgss: any) {
        let uri = this.url + "/igss/" + idIgss;
        let data = JSON.stringify(igss);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http.put(uri, data, { headers })
            .map(res => {
                return res.json();
            });
    }

    public deleteIgss(idIgss: any) {
        let uri = this.url + "/igss/" + idIgss;
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.delete(uri, headers)
            .map(res => {
                return res.json();
            })
    }
}