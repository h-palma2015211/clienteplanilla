import {Injectable} from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class SalarioOrdinarioService{
      url:string = "http://localhost:3001";
    constructor(
        private http:Http
    ){}

    public getSalarios() {
        let uri = `${this.url}/salarioOrdinario`;
        console.log(uri);
        return this.http.get(uri)
        .map(res => {
            return res.json();
        })
    }

    public getSalario(id : any) {
        let uri = this.url + "/salarioOrdinario/" + id;
        console.log(uri);
        return this.http.get(uri)
        .map( res => {
            console.log(res.json());
            return res.json(); 
        });
    }

    public insertSalario(salario : any) {
        let uri = this.url + "/salarioOrdinario";
        let data = JSON.stringify(salario);
        let headers = new Headers({
        'Content-Type': 'application/json'
        });
        return this.http.post(uri, data,{headers})
        .map(res => {
            return res.json();
        })
    }

    public updateSalario(salario : any, idSalario: any) {
        let uri = this.url + "/salarioOrdinario/" + idSalario;
        console.log(uri);
        let data = JSON.stringify(salario);
        let headers = new Headers({
        'Content-Type': 'application/json'
        });
        return this.http.put(uri, data,{headers})
        .map( res => {
            res.json();
        })
    }

    public deleteSalario(idSalario : any) {
        let uri = this.url + "/salarioOrdinario/" + idSalario;
        console.log(uri);
        let headers = new Headers({
        'Content-Type': 'application/json'
        });
        
        return this.http.delete(uri, {headers})
        .map( res => {
            return res.json();
        })
    }

}