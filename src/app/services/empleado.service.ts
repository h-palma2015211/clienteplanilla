import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class EmpleadoService {
    url: string = "http://localhost:3001";
    constructor(private http: Http) { }

    public getEmpleados() {
        let uri = `${this.url}/empleado`;
        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public getEmpleado(idEmpleado: any) {
        let uri = this.url + "/empleado/" + idEmpleado;
        console.log(uri);
        return this.http.get(uri)
            .map(res => {
                console.log(res.json());
                return res.json();
            })
    }

    public getHistorial1(idEmpleado: any) {
        let uri = this.url + "/empleado/historial1/" + idEmpleado;
        console.log(uri);
        return this.http.get(uri)
            .map(res => {
                return res.json();
            })
    }


    public getHistorial2(idEmpleado: any) {
        let uri = this.url + "/empleado/historial2/" + idEmpleado;
        console.log(uri);
        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public insertEmpleado(empleado: any) {
        let uri = this.url + "/empleado";
        let data = JSON.stringify(empleado);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http.post(uri, data, { headers })
            .map(res => {
                return res.json();
            })
    }


    public updateEmpleado(empleado: any, idEmpleado: any) {
        let uri = this.url + "/empleado/" + idEmpleado;
        console.log(uri);
        let data = JSON.stringify(empleado);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.put(uri, data, { headers })
            .map(res => {
                return res.json()
            });
    }

    public deleteEmpleado(idEmpleado: any) {
        let uri = this.url + "/empleado/" + idEmpleado;
        return this.http.delete(uri)
            .map(res => {
                return res.json();
            });
    }
}