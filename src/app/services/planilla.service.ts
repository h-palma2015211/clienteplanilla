import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class PlanillaService {
    url: string = "http://localhost:3001";
    constructor(private http: Http) { }


    public getPlanilla(cont: any) {
        let uri = this.url + "/planillabuscar";
        let data = JSON.stringify(cont);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http.post(uri, data, { headers })
            .map(
            res => {
                return res.json();
            }
            );

    }
    public getDatos(cont: any) {
        let uri = this.url + "/planillaObtener";
        let data = JSON.stringify(cont);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.post(uri, data, { headers })
            .map(
            res => {
                return res.json();
            }
            );
    }

    public guardarPla(contenido: any) {
        let uri = this.url + "/planillaAgregar";
        let data = JSON.stringify(contenido);
        let headers = new Headers({
            'Content-Type': 'application/json'
        });

        return this.http.post(uri, data, { headers })
            .map(res => {
                return res.json();
            })
    }
    public guardarDetalle(contenido: any) {
        let uri = this.url + "/plDetalleAgregar";
        let data = {
            totalSalario: contenido.totalSueldo,
            sueldoLiquido: contenido.liquido,
            salarioOrdinario: contenido.salario,
            igss: contenido.cuotaIgss,
            idEmpleado: contenido.idEmpleado,
            bonificacion: contenido.coutaBonificacion,
            retencion: contenido.cuotaRetencion
        }
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let datos = JSON.stringify(data);
        console.log(datos);
        return this.http.post(uri, datos, { headers })
            .map(res => {
                return res.json();
            });
    }


    public historial(contenido: any) {
        let uri = this.url + "/planilla/" + contenido;
        console.log(uri);

        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public eliminar(id: any) {
        console.log(id);
        let uri = this.url + "/planilla/" + id;
        console.log(uri);

        return this.http.delete(uri)
            .map(res => {
                return res.json();
            });

    }
    public detalles(id: any) {
        let uri = this.url + "/detalles/" + id;
        console.log(uri);
        return this.http.get(uri)
            .map(res => {
                return res.json();
            });
    }

    public update(cont: any, id: any) {
        let uri = this.url + "/detalles/" + id;
        console.log(uri);

        let data = JSON.stringify(cont);
        console.log(data);
        return this.http.put(uri, cont)
            .map(res => {
                return res.json();
            });
    }

    public detalle(id: any) {
        let uri = this.url + "/detalle/" + id;
        return this.http.get(uri)
            .map(res => {
                console.log(res.json());
                return res.json();
            });
    }

}