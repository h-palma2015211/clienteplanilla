//imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { app_routing } from './app.routes';
//componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IgssComponent } from './igss/igss.component';
import { SalarioOrdinarioComponent } from './salario-ordinario/salario-ordinario.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { PlanillaComponent } from './planilla/planilla.component';
import { HomeComponent } from './home/home.component';
import { DetalleSalarioOrdinarioComponent } from "./salario-ordinario/detalle-salario-ordinario.component";
import { DetalleEmpleadoComponent } from "./empleado/detalle-empleado.component";

//servicios
import { SalarioOrdinarioService } from "./services/salarioOrdinario.service";
import { IgssService } from "./services/igss.service";
import { DetalleIgssComponent } from "./igss/detalle-igss.component";
import { EmpleadoService } from "./services/empleado.service";
import { HistorialEmpleadoComponent } from "./empleado/historial-empleado.component";
import { PlanillaService } from "./services/planilla.service";
import { HistorialComponent } from './historial/historial.component';
import { DetalleHistorialComponent } from "./historial/detalle-historial.component";
import { EditarHistorialComponent } from "./historial/editar-historial.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IgssComponent,
    SalarioOrdinarioComponent,
    EmpleadoComponent,
    PlanillaComponent,
    HomeComponent,
    DetalleSalarioOrdinarioComponent,
    DetalleIgssComponent,
    DetalleEmpleadoComponent,
    HistorialEmpleadoComponent,
    HistorialComponent,
    DetalleHistorialComponent,
    EditarHistorialComponent
  ],
  imports: [
     BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    app_routing
  ],
  providers: [
    SalarioOrdinarioService,
    IgssService,
    EmpleadoService,
    PlanillaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
